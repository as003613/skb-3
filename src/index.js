import express from 'express';
import cors from 'cors';
import 'isomorphic-fetch';

const app = express();
app.use(cors());


function getVolumes(hdd = []) {
  let volumes = {};
  hdd.forEach(value => {
    if (volumes[value.volume]) {
      volumes[value.volume] = `${+value.size + (+volumes[value.volume].slice(0, -1))}B`;
    }
    else {
      volumes[value.volume] = `${value.size}B`;
    }
    console.log('volumes', volumes);
  });

  return volumes;
}

app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task3A/volumes', (req, res) => {
  return res.json(getVolumes(pc.hdd));
});

app.get('/task3A/*', (req, res) => {
  console.log('req.params', req.params);
  let params = req.params[0].split('/').filter(v => v);
  console.log('params', params);
  try {
    let result = params.reduce((prevVal, currVal) => prevVal.hasOwnProperty(currVal)
      && !((Array.isArray(prevVal) || typeof (prevVal) == 'string') && currVal == 'length')
      ? prevVal[currVal]
      : undefined, pc);
    console.log('result', result);
    if (result === undefined) {
      throw 'undefined';
    }
    return res.json(result);
  }
  catch (err) {
    return res.status(404).send('Not Found');
  }
});

let pc = {};
(function () {
  const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

  return fetch(pcUrl)
    .then(async (res) => {
      pc = await res.json();
    })
    .catch(err => {
      console.log('Что-то пошло не так:', err);
    });
})()
  .then(() => {
    app.listen(3000, () => {
      console.log('pc', pc);
      console.log('Your app listening on port 3000!');
    });
  });

